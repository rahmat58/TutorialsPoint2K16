<?php
   
    class Crud{

      private $db;

      public function __construct($db){
          $this->db = $db;
      }

      public function dataview($query){
          $stmt = $this->db->prepare($query);
          $stmt->execute();

          if ($stmt->rowCount()>0) {
          	 while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          ?>

           <tr>
           	  <td><?php if(isset($row['id'])) {echo ($row['id']);}?></td>
           </tr>

 <?php
          	 }
          }
      }
    }

?>